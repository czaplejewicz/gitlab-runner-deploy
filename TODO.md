Things to do
============

Docker-rebuild
--------------

The `docker-rebuild` service will remove volume which are in use.

Pruning volumes should be decoupled from rebuilding images, and conditional:

- only when no gitlab CI is running (iow, no live containers)
- optionally, only when the volumes take up lots of space

These volumes contain build files, and possibly artifacts.

Define CFM installation.
------------------------

Needed work:

- copy the description from `.odt` document to a script
- fix autosuspend (https://badlinuxadvice.wordpress.com/2015/01/31/fixing-automatic-suspend-for-linux-mint-17-1-cinnamon/)
- enable autosuspend
- enable automatic login? (https://help.ubuntu.com/community/AutoLogin) **OR** give `cfm` user a simple password and disable SSH for `cfm` user
- disable screen lock (*dconf* `org.cinnamon.desktop.screensaver.lock-enabled`), see https://github.com/jistr/ansible-dconf
