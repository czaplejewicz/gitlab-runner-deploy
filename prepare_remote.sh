HOSTNAME=$1
SSH_KEY=`cat ~/.ssh/id_rsa.pub`

echo "mkdir ~/.ssh
echo $SSH_KEY > ~/.ssh/authorized_keys
chmod -R go-rw ~/.ssh
mv /etc/ssh/sshd_config{.bak,}
systemctl restart sshd" | ssh root@$HOSTNAME
