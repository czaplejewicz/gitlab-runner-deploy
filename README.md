Runner-deploy
=============

Everything you need to start a MorphoGraphX CI runner.

Contains:

- `play-noimage.yml`: Ansible playbook to deploy a Gitlab Docker runner on Debian 9.
- `play.yml`: Ansible playbook to deploy MGX-compatible runner

To install a MorphoGraphX-compatible runner, currently it's only necessary to use `play.yml`.

*Note*: Don't forget to adjust `config.yml.example` and copy it as `config.yml`. See below for details.

Operation
---------

CI runners will run GitLab CI jobs.

Each runner by default can run a maximum of 2 jobs at the same time (to avoid IO congestion).

Runners expose an environment variable THREADCOUNT, which can be used to set the optimal number of threads in the job (by default: CPU threads * 3/4).
The value was chosen because modern x64 CPU compile speed for MGX seems to plateau at thread number > CPU threads / 2 (as tested on i7-3770).

Runners also contain and rebuild (weekly) Docker images with MGX dependencies embedded:

- `mgx_devel_debian9` - current `debian:9` + deps
- `mgx_devel_debian8` - current `debian:8` + deps + CUDA
- `mgx_devel_mint18` - current `vcatechnology/linux-mint:18` + deps
- `mgx_devel_cuda_mint18` - `mgx_devel_mint18` + CUDA

Preparation
-----------

### Runner machine

1. Install Debian as usual (remember root password)
- enter mirror manually: deb.debian.org
- select SSH server and basic system utilities
2. Log in as root
- save IP address (`ip addr`) or host name
- `cp /etc/ssh/sshd_config{,.bak}`
- install `vim` if necessary (`apt-get install vim`) and edit `/etc/ssh/sshd_config`. Make sure there is a line saying:

```
PermitRootLogin yes
```

- do `systemctl restart sshd`

### Control machine

Copy the file `config.yml.example` to `config.yml` and fill it in with [gitlab registration host and token](https://docs.gitlab.com/ce/ci/runners/#registering-a-specific-runner-with-a-project-registration-token) in project's *settings* → *pipelines*.

**Note**: The BOINC numbers are optional - only needed for donating computer time for research - see below.

Generate SSH key for accessing the remote host, if there isn't one already. Copy it to remote host:

```
sh ./prepare_remote.sh $HOSTNAME # this will ask for root password
```

Install the `ansible` package. Then:

```
sudo ansible-galaxy install debops.core debops.unattended_upgrades debops.gitlab_runner
git clone https://github.com/dcz-mpipz/ansible-gitlab_runner.git
```

Edit `/etc/ansible/hosts` and add an entry in `[buildservers]` section, like this:

```
[buildservers]
$HOSTNAME ansible_user=root
```

Optionally, specify level of parallelism for jobs:

```
$HOSTNAME ansible_user=root runner_threadcount=6
```

Check with:

```
ansible buildservers -m ping
```

Installation
------------

Install together with MGX dev image (slow):

```
ansible-playbook play.yml
```

BOINC
-----

[BOINC](http://boinc.berkeley.edu/) donates computer time to researchers at various institutions, [including MPI for Gravitational Physics](https://einsteinathome.org/about) via [Einstein@Home](https://einsteinathome.org/).

It is not required for a build server.

The `boinc.yml` playbook will turn on BOINC outside working hours.

```
sudo ansible-galaxy install GROG.boinc
ansible-playbook boinc.yml
```

### Configuration

Put "project url" and "weak account key" of your account into `config.yml`. Each project needs a separate one.

NVIDIA
------

Installs the NVIDIA driver + CUDA. By default, installs to all hosts - limit as needed. Useful for BOINC and workstations, less useful for build machines.

```
ansible-playbook nvidia.yml
```

**Note**: if the driver was "changed", then the computer needs a reboot.

The MGX development images
--------------------------

These images are a Docker images with MGX dependencies and optionallt CUDA pre-installed.

Once installed, they are kept up to date automatically for a given computer. They must be present for MorphoGraphX CI pipelines.

They get installed automatically with `play.yml` playbook. If you need to install them manually, run `docker-rebuilds.yml` playbook.
